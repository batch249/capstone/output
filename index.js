class Customer {
	constructor(email){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut(){
		console.log(`Checking out...`);

		if (this.cart.contents.length !== 0) {
			this.orders.push({
				products: this.cart.contents,
				totalAmount: this.cart.totalAmount
			})
		}
	}
}

class Cart {
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, quantity){
		console.log(`Adding to cart: ${product.name} : ${quantity}`); 
		this.contents.push({
			product : product,
			quantity : quantity
		})
		return this;
	}

	showCartContents(){
		this.contents.forEach(content => console.log(content));
	}

	updateProductQuantity(productName, newQuantity) {
		console.log(`Updating the cart: ${productName} : ${newQuantity}`);
		let index = this.contents.findIndex(content => content.product.name === productName);
		this.contents[index].product.name = productName;
		this.contents[index].quantity = newQuantity;
		return this;

	}

	clearCartContents() {
		console.log(`Clearing the cart..`);
		this.contents = [];
		this.totalAmount = 0;
		return this;
	}

	computeTotal() {
		let sum = 0;
		this.contents.forEach(content => sum += content.product.price * content.quantity);
		this.totalAmount = sum;
	}
}

class Product {
	constructor(name, price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(){
		this.isActive = false;
	}

	updatePrice(newPrice){
		this.price = newPrice;
	}

}


//TEST
const john = new Customer('john@mail.com');
console.log(john);

const prodA = new Product('soap', 9.99);
console.log(prodA);

prodA.updatePrice(12.99);
console.log(prodA);

prodA.archive();
console.log(prodA);

john.cart.addToCart(prodA, 3).computeTotal();

console.log(`Cart total is: ${john.cart.totalAmount}`);

john.cart.updateProductQuantity('soap', 5).computeTotal();
console.log(`Cart total is: ${john.cart.totalAmount}`);

john.cart.clearCartContents().computeTotal();
console.log(`Cart total is: ${john.cart.totalAmount}`);

//Try adding 1 more product
const prodB = new Product('lotion', 11);
console.log(prodB);
john.cart.addToCart(prodB, 3).computeTotal();

const prodC = new Product('perfume', 15);
console.log(prodC);
john.cart.addToCart(prodC, 1).computeTotal();

john.checkOut();
console.log(john);
console.log(`Order total: ${john.orders[0].totalAmount}`);